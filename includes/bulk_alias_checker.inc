<?php

/**
 * @file
 * Inc file for Bulk alias checker.
 */

/**
 * Implements bulk_alias_checker_form().
 */
function bulk_alias_checker_form($form, &$form_state) {
  $form['bulk_alias_checkerholder'] = array(
    '#type' => 'fieldset',
    '#description' => t('Upload a CSV file.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['bulk_alias_checkerholder']['file_upload'] = array(
    '#type' => 'file',
    '#title' => t('CSV File'),
    '#size' => 40,
    '#description' => t('Select the CSV file to check whether Alias are exist or not.'),
    '#upload_validators' => array('file_validate_extensions' => array('csv')),
  );

  $form['bulk_alias_checkerholder']['sample_file'] = array(
    '#markup' => '<p>' . l(t('Click here to download sample file'), 'bulk_alias_checker/sample-file') . '</p>',
  );

  $form['bulk_alias_checkerholder']['submit_button'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  return $form;
}

/**
 * Implements bulk_alias_checker_form_validate().
 */
function bulk_alias_checker_form_validate($form, &$form_state) {
  // Attempt to save the uploaded file.
  $validators = array('file_validate_extensions' => array('csv'));

  $file = file_save_upload('file_upload', $validators);

  if (!$file) {
    form_set_error('file_upload', t('A file must be uploaded or selected.'));
  }
  elseif ($file->filemime != 'text/csv') {
    form_set_error('file_upload', t('The file must be of CSV type only.'));
  }
  else {
    // Set files to form_state, to process when form is submitted.
    $form_state['values']['file_upload'] = $file;
  }
}

/**
 * Implements bulk_alias_checker_form_submit().
 */
function bulk_alias_checker_form_submit($form, &$form_state) {
  $line_max = variable_get('bulk_alias_checker_import_line_max', 2000);
  $filepath = $form_state['values']['file_upload']->uri;
  $handle = fopen($filepath, 'r');
  $send_counter = 0;
  $data = array();
  while ($row = fgetcsv($handle, $line_max, ',')) {
    if ($send_counter != 0) {
      $data[] = $row;
    }
    $send_counter++;
  }

  $operations = array();

  foreach ($data as $csvdata) {
    $first_string = trim($csvdata[0]);
    if (!empty($first_string) && isset($first_string)) {
      $operations[] = array('bulk_alias_checker_op', array($csvdata));
    }
  }

  $batch = array(
    'title' => t('Checking the node alias finally....please wait....!'),
    'operations' => $operations,
    'finished' => 'bulk_alias_checker_op_batch_finished',
    'init_message' => t('Node alias checking started...please wait....!'),
    'progress_message' => t('Processed @current out of @total.'),
    'error_message' => t('Node alias checking process has encountered an error.'),
    'file' => drupal_get_path('module', 'bulk_alias_checker') . '/includes/bulk_alias_checker.inc',
  );

  batch_set($batch);
  batch_process('admin/config/search/bulk_alias_checker');
}

/**
 * Implements bulk_alias_checker_op().
 */
function bulk_alias_checker_op($csvdata, &$context) {
  $message = '';
  if (isset($csvdata[0]) && !empty($csvdata)) {
    $check = bulk_alias_checker_exist_not($csvdata[0]);

    if ($check == TRUE) {
      $message = t('URL Found: @csvdata', array('@csvdata' => $csvdata[0]));
      drupal_set_message($message);
    }
    else {
      $message = t('URL Found: @csvdata', array('@csvdata' => $csvdata[0]));
      drupal_set_message($message, 'warning');
    }
  }
}

/**
 * Implements bulk_alias_checker_op_batch_finished().
 */
function bulk_alias_checker_op_batch_finished($success, $results, $operations) {
  if ($success) {
    drupal_set_message(t('Url processed successfully.'));
  }
  else {
    drupal_set_message(t('An error occurred while processing.'));
  }
}

/**
 * Implements bulk_alias_checker_exist_not().
 */
function bulk_alias_checker_exist_not($url) {
  if (bulk_alias_checker_existence($url)) {
    return TRUE;
  }
  return FALSE;
}

/**
 * Implements bulk_alias_checker_existence().
 */
function bulk_alias_checker_existence($alias) {
  $sql = db_select('url_alias', 'ua');
  $sql->fields('ua', array('pid'));
  $sql->condition('ua.alias', $alias);
  return $sql->execute()->fetchField();
}

/**
 * Implements bulk_alias_checker_sample_file().
 */
function bulk_alias_checker_sample_file() {
  $file = drupal_get_path('module', 'bulk_alias_checker') . '/samplefile/bulk_alias_checker.csv';
  header("Content-type: octet/stream");
  header("Content-disposition: attachment; filename=" . $file . ";");
  header("Content-Length: " . filesize($file));
  readfile($file);
  exit;
}
