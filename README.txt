INTRODUCTION
------------

This module checks whether the aliases exists or not in drupal application
through CSV import, and CSV format have been attached with this module.


INSTALLATION
------------
 
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


CONFIGURATION
-------------

* The module has menu. There is no configuration. When
  enabled, the module will prevent the links from appearing.
  To get the links back, disable the module and clear caches.

* Change sample file as per your requirement from below mentioned file.
  bulk_alias_checker/samplefile/alias_checker.csv 

* You can find the menu here

  Configuration >> SEARCH AND METADATA >> Bulk Alias Checker

  A form will be visible once you reach this path.
  Now change import CSV and check aliases.
